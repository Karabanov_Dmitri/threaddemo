package ru.demo.currenttread;

/**
 * Класс, демонстрирующий ипользования
 * методов класса Tgread
 * в главном потоке программы
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        //переменная hread ссылка на главный поток программы
        Thread thread = Thread.currentThread();
        //Bывод сведений о гавном потоке
        System.out.println("текущий поток" + thread);
        System.out.println("имя потока:" + thread.getName());
        System.out.println("реоритет потока" + thread.getPriority());
        System.out.println("группа потока" + thread.getThreadGroup());
        System.out.println("идентефикатор потока:" + thread.getId());
        System.out.println("состояние потока:" + thread.getState());
        thread.setName("Главный поток");
        thread.setPriority(10);
        System.out.println("теперь текущий поток: " + thread);
        //вывод цифры с задержкой потока на1 секнду
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                System.out.println("поток завершен");
            }
        }
    }
}
