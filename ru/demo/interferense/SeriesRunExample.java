package ru.demo.interferense;

/**
 *
 */
public class SeriesRunExample extends Thread {
    private static int currentMax = 1;
    private int mainId;
    private final Object waitObject;

    public SeriesRunExample(int mainId, Object waitObject) {
        this.mainId = mainId;
        this.waitObject = waitObject;
    }

    public static void example(){
        Object waitObject = new Object();
        for (int i = currentMax; i < 10; ++i) {
            Thread thread = new SeriesRunExample(i, waitObject);
            thread.start();
        }
    }

    public void run() {
        System.out.println("стартовал поток " + mainId);
        //запускаются в беспорядки
        synchronized (waitObject){
            while (mainId > currentMax){
                try {
                    waitObject.wait();
//                    System.out.println("после wait'a " + mainId);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            currentMax++; //сюда приходят по порядку
            System.out.println("отработал поток " + mainId);
            waitObject.notifyAll();
        }
    }
}
