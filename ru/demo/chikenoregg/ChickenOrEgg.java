package ru.demo.chikenoregg;

class Egg implements Runnable {
    public void run() {
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);//устанавливает приоритет потока 10
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);//Приостанавливает поток на 1 секунду
                System.out.println("яйцо!");
            } catch (InterruptedException e) {
            }
//Слово «яйцо» сказано 10 раз
        }
    }
}

class Chicken {
    public static void main(String[] args) {
        System.out.println("Спор начат...");
        (new Thread(new Egg())).start();//Запуск потока
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);//устанавливает приоритет потока 1
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);//Приостанавливает поток на 1 секунду
            } catch (InterruptedException e) {
            }
            System.out.println("курица!");//Слово «курица» сказано 10 раз
        }

        if ((new Thread(new Egg())).isAlive()) {//Если оппонент еще не сказал последнее слово
            try {
                new Thread(new Egg()).join();//Подождать пока оппонент закончит высказываться.
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Первым появилось яйцо!");//если оппонент уже закончил высказываться
        } else {
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}

