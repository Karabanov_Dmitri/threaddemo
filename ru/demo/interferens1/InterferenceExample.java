package ru.demo.interferens1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Пример для сравнения рузультатов инкремента переменной
 */
class InterferenceExample {
    private static final int HUNDRED= 200000;
    private AtomicInteger counter = new AtomicInteger();

    boolean stop() {
        return counter.incrementAndGet() > HUNDRED;
    }

    void example() throws InterruptedException {
        InterferenceThread thread1 = new InterferenceThread("поток 1", this);
        InterferenceThread thread2 = new InterferenceThread("поток 2", this);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("ожидание: "+HUNDRED);
        System.out.println("получили: " + InterferenceThread.getCounter());
    }
}