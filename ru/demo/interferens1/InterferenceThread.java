package ru.demo.interferens1;

import java.util.concurrent.atomic.AtomicInteger;

public class InterferenceThread extends Thread {
    private static volatile AtomicInteger counter=new AtomicInteger();

    private final InterferenceExample checker;

    InterferenceThread(String name, InterferenceExample checker) {
        super(name);
        this.checker = checker;
    }

    public void run() {
        System.out.println(this.getName() + " запущен");
        while (!checker.stop()) {
            increment();
        }
        System.out.println(this.getName() + "закончен");
    }

    private int increment() {
        return counter.incrementAndGet();
    }

   public static AtomicInteger getCounter(){
        return counter;
   }

}
