package ru.demo.ReadAndWrite;


/**
 * Файл запуска
 *
 * @author Карабанов
 */
public class Main extends Thread {
    private static final String WayToData = "src\\ru\\demo\\ReadAndWrite\\DataFile 1.dat";
    private static final String WayToData1 = "src\\ru\\demo\\ReadAndWrite\\DataFile 2.dat";

    public static void main(String[] args) {
        Thread thread = new FileReader(WayToData);
        Thread thread1 = new FileReader(WayToData1);
        thread.start();
        thread1.start();
    }

}
