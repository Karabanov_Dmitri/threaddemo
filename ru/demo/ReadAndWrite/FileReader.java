package ru.demo.ReadAndWrite;

import java.io.*;

/**
 *класс читающий из файла и передающий строку в класс FileWriter
 *
 * @author Карабанов
 */
public class FileReader extends Thread {
    private String WayToData ;
    FileReader(String WayToData){
        this.WayToData=WayToData;
    }

    @Override
    public void run() {
        try(BufferedReader bufferedReader =new BufferedReader(new java.io.FileReader(WayToData))) {
            final long before=System.currentTimeMillis();
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                FileWriter.writer(data);
                yield();
            }
            final long after=System.currentTimeMillis();
            System.out.println("name = "+getName()+"\n time = "+(after-before)/1000+"ns");
        } catch (IOException f) {
            System.out.println("ОШИБКА" + f);
        }

    }
}
