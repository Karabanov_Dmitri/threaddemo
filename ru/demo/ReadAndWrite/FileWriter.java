package ru.demo.ReadAndWrite;

import java.io.*;

/**
 *класс записи в файл Writer.dat
 * @author Карабанoв
 */
class FileWriter extends Thread {
    private static final String WayToResult = "src/ru/demo/ReadAndWrite/Writer.dat";

    /**
     * @param toPrint строка
     */

    static synchronized void writer(String toPrint) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new java.io.FileWriter(WayToResult, true))) {
            bufferedWriter.write(toPrint + "\n");
        } catch (IOException e) {
            System.out.println("ошибка" + e);
        }
    }
}
