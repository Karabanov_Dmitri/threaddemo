package ru.demo.music;

/**
 * Класс запуска
 *
 * @author Карабанов
 */

public class Main extends Thread {

    private static final String IN_FILE_TXT = "src\\ru\\demo\\music\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\demo\\music\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\demo\\music\\music\\music";

    public static void main(String[] args) throws InterruptedException {
        try {
            final long time = System.currentTimeMillis();
            Thread musicThread = new UrlReader(IN_FILE_TXT, OUT_FILE_TXT, PATH_TO_MUSIC);
            musicThread.start();
            musicThread.join();
            System.out.println("Общее время = " + ((System.currentTimeMillis() - time)));
        } catch (InterruptedException e) {
            System.out.println("error" + e);
        }
    }
}
