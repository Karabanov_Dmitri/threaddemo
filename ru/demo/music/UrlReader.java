package ru.demo.music;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UrlReader extends Thread {
    private String inFileTxt;
    private String outFileTxt;
    private String pathToMusic;

    UrlReader(String inFileTxt, String outFileTxt, String pathToMusic) {
        this.inFileTxt = inFileTxt;
        this.outFileTxt = outFileTxt;
        this.pathToMusic = pathToMusic;
    }


    @Override
    public void run() {
        try (BufferedReader inFile = new BufferedReader(new FileReader(inFileTxt))) {
            System.out.println("Осуществляется чтение ссылки");
            String result, url1;
            while ((url1 = inFile.readLine()) != null) {
                URL url = new URL(url1);
                result = input(url);
                urlPattern(result, outFileTxt);
            }
            linkReader();
            join();
        } catch (IOException e) {
            System.out.println("проверьте соединение интернета!!!!!!!!!!!!!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * метод выполняющий поиск адреса
     */
    private static void urlPattern(String result, String OutFileTxt) {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(OutFileTxt))) {
            System.out.println("Ведется поиск ссылок на скачивание");
            Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*/*(?=\")");
            Matcher matcher = email_pattern.matcher(result);
            for (int i = 0; matcher.find() && i < 10; i++) {
                outFile.write(matcher.group() + "\n");
            }
        } catch (IOException e) {
            System.out.println("error" + e);
        }
    }

    /**
     * Метод считывает ссылки для скачивания музыки из текстового файла OUT_FILE_TXT.
     */

    private void linkReader() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(outFileTxt))) {
            String music;
            for (int count = 0; (music = musicFile.readLine()) != null; count++) {
                Dowload downloader = new Dowload(music, pathToMusic + String.valueOf(count) + ".mp3");
                downloader.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Метод считывает исходный код html страниц по ссылке.
     * @param url ссылка
     * @return result
     */
    private String input(URL url) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}






