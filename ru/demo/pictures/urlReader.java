package ru.demo.pictures;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class urlReader extends Thread {
    private String InFileTxt;
    private String OutFileTxt;
    private String PathToMusic;

    urlReader(String inFileTxt, String outFileTxt, String pathToMusic) {
        this.InFileTxt = inFileTxt;
        this.OutFileTxt = outFileTxt;
        this.PathToMusic = pathToMusic;
    }


    @Override
    public void run() {
        try (BufferedReader inFile = new BufferedReader(new FileReader(InFileTxt))) {
            System.out.println("Осуществляется чтение ссылки");
            String result, Url;
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);
                result = input(url);
                urlPattern(result, OutFileTxt);
            }
            linkReader();
        } catch (IOException e) {
            System.out.println("проверьте соединение интернета!!!!!!!!!!!!!");
        }
    }

    /**
     * метод выполняющий поиск адреса
     */
    private static void urlPattern(String result, String outFileTxt) {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(outFileTxt))) {
            System.out.println("Ведется поиск ссылок на скачивание");
            Pattern email_pattern = Pattern.compile("\\s*(?<=src\\s?=\\s?\")[^\\/][^>]*\\/*[^\"](((jpg)|(png))|gif)(?=\")");
            Matcher matcher = email_pattern.matcher(result);
            for (int i = 0; matcher.find() && i < 10; i++) {
                outFile.write(matcher.group() + "\n");
            }
        } catch (IOException e) {
            System.out.println("error" + e);
        }
    }

    /**
     * Метод считывает ссылки для скачивания картинок из текстового файла OUT_FILE_TXT.
     */

    private void linkReader() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OutFileTxt))) {
            String music;
            for (int count = 0; (music = musicFile.readLine()) != null; count++) {
                Dowload downloader = new Dowload(music, PathToMusic + String.valueOf(count) + ".jpg");
                downloader.start();
                downloader.join();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод считывает исходный код html страниц по ссылке.
     */
    private String input(URL url) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}





