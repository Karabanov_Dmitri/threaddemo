package ru.demo.pictures;

/**
 * Класс запуска
 *
 * @author Карабанов
 */

public class Main extends Thread {

    private static final String IN_FILE_TXT = "src\\ru\\demo\\pictures\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\demo\\pictures\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\demo\\pictures\\pictures\\pictures";

    public static void main(String[] args) throws InterruptedException {
        try {
            final long time = System.currentTimeMillis();
            Thread musicthread = new urlReader(IN_FILE_TXT, OUT_FILE_TXT, PATH_TO_MUSIC);
            musicthread.start();
            musicthread.join();
            System.out.println("Общее время = " + ((System.currentTimeMillis() - time)));
        } catch (InterruptedException e) {
            System.out.println("error" + e);
        }
    }
}
