package ru.demo.pictures;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;


/**
 * класс скачивания картинок
 *
 * @author Карабанов
 */
public class Dowload extends Thread {

    private String strUrl;
    private String file;


    public Dowload(String strUrl, String file) {
        this.strUrl = strUrl;
        this.file = file;
    }


    /**
     * Метод, осуществляющий скачивание файлов
     */
    @Override
    public void run() {
        try {
            URL url = new URL(this.strUrl);
            try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream()); FileOutputStream stream = new FileOutputStream(file);) {
                System.out.println("Ведется скачивание файла " + file);
                stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


