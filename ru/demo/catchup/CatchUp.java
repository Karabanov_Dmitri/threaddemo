package ru.demo.catchup;

/**
 * рабочий класс догонялок
 *
 * @author Карабанов 15ит18
 */
public class CatchUp extends Thread {
    private String name;
    private int prior;

    /**
     * конструктор
     *
     * @param name  имя участника
     * @param prior изменения в приоритете
     */
    public CatchUp(String name, int prior) {
        this.name=name;
        this.prior = prior;
    }

    /**
     * Констуктор по умолчанию
     */

    public CatchUp() {
        this("Серёжа", 1);
    }

    @Override
    public void run() {
        for (int i = 0; i < 5_000; i++) {
            try {
                sleep(100);//Приостанавливает поток на 0.1 секунду
            } catch (InterruptedException e) {
                System.out.println("ошибка");
            }
            if (i == 2_000) {
                Thread.currentThread().setPriority(prior);//устанавливает приоритет потока
            }
            System.out.println(name);
        }
    }
}