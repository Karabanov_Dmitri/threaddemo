package ru.demo.catchup;


/**
 * класс для запуска 2 потоков
 *
 * @author Карабанов 15ит18
 */
public class Demo {

    public static void main(String[] args) {
        CatchUp catchUp = new CatchUp("Серёжа", 10);
        CatchUp catchUp1 = new CatchUp("Дима", 1);
        catchUp.setPriority(1);
        catchUp.start();
        catchUp1.setPriority(10);
        catchUp1.start();
    }
}
