package ru.demo.copyfile.paral;

/**
 * паралельное копирование файла
 */
public class Demo extends Thread {
    private static final String  WayToData = "src/ru/demo/copyfile/DataFile 1.dat";
    private static final String WayToData1="src/ru/demo/copyfile/DataFile 2.dat";
    private static final String WayToResult1 = "src/ru/demo/copyfile/writer.dat";

    public static void main(String[] args) throws InterruptedException {
        long time = System.currentTimeMillis();
        Thread thread = new FileReader( WayToData, WayToResult1);
        Thread thread1 = new FileReader( WayToData1, WayToResult1);
        thread.start();
        thread1.start();
        thread.join();
        thread1.join();
        System.out.println("Общее время = " + (System.currentTimeMillis() - time));

    }

}
