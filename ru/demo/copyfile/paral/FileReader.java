package ru.demo.copyfile.paral;

import java.io.*;

/**
 * Считывающий файл
 *
 * @author Карабанов
 */
public class FileReader extends Thread {
    private String WayToData;
    private String WayToResult;

    FileReader(String WayToData, String WayToResult) {
        this.WayToData = WayToData;
        this.WayToResult = WayToResult;
    }


    @Override
    public void run() {
        try (BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(WayToData))) {
            final long before = System.currentTimeMillis();
            String data;
            while ((data = bufferedReader.readLine()) != null) {
                try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(WayToResult, true))) {
                    bufferedWriter.write(data + "\n");
                } catch (IOException e) {
                    System.out.println("ошибка" + e);
                }
            }
            final long after = System.currentTimeMillis();
            System.out.println("name = " + getName() + "\n time = " + (after - before));
        } catch (IOException f) {
            System.out.println("ОШИБКА" + f);
        }

    }
}
